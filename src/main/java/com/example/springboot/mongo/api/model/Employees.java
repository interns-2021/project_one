package com.example.springboot.mongo.api.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Document(collection = "Employees")



public class Employees {
	@Id
	public int id;
	public String EmployeeName;
	public String CityName;
	

}
